import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {

    @Test
    public void testGetX1() {
        Vector3d vecToCheckX = new Vector3d(2, 5, 6);
        assertEquals(2, vecToCheckX.getX());
    }

    @Test
    public void testGetX2() {
        Vector3d vecToCheckX = new Vector3d(-1, 5, 6);
        assertEquals(-1, vecToCheckX.getX());
    }

    @Test
    public void testGetY1() {
        Vector3d vecToCheckY = new Vector3d(2, 5, 6);
        assertEquals(5, vecToCheckY.getY());
    }

    @Test
    public void testGetY2() {
        Vector3d vecToCheckY = new Vector3d(2, 0, 6);
        assertEquals(0, vecToCheckY.getY());
    }

    @Test
    public void testGetZ() {
        Vector3d vecToCheckZ = new Vector3d(2, 5, 6);
        assertEquals(6, vecToCheckZ.getZ());
    }

    @Test
    public void testMagnitudeMethod1() {
        Vector3d vc = new Vector3d(1, 2, 3);
        double magnitudResult = vc.magnitude();
        assertEquals(3.741, magnitudResult, 3.8);
    }

    @Test
    public void testMagnitudeMethod2() {
        Vector3d vc = new Vector3d(1, 0, 0);
        double magnitudResult = vc.magnitude();
        assertEquals(1, magnitudResult);
    }

    @Test
    public void testdotMethod1() {
        Vector3d vc1 = new Vector3d(1, 2, 3);
        Vector3d vc2 = new Vector3d(1, 2, 3);
        double dotMethodResult = vc1.dotProduct(vc2);
        assertEquals(14, dotMethodResult);
    }

    @Test
    public void testdotMethod2() {
        Vector3d vc1 = new Vector3d(0, 2, 3);
        Vector3d vc2 = new Vector3d(1, 2, -1);
        double dotMethodResult = vc1.dotProduct(vc2);
        assertEquals(1, dotMethodResult);
    }

    @Test
    public void testAddMethod1() {
        Vector3d vc1 = new Vector3d(1, 2, 3);
        Vector3d vc2 = new Vector3d(1, 2, 3);
        Vector3d addMethodResult = vc1.add(vc2);
        assertEquals(2, addMethodResult.getX());
        assertEquals(4, addMethodResult.getY());
        assertEquals(6, addMethodResult.getZ());
    }

    @Test
    public void testAddMethod2() {
        Vector3d vc1 = new Vector3d(0, -2, 3);
        Vector3d vc2 = new Vector3d(1, 1, -3);
        Vector3d addMethodResult = vc1.add(vc2);
        assertEquals(1, addMethodResult.getX());
        assertEquals(-1, addMethodResult.getY());
        assertEquals(0, addMethodResult.getZ());
    }

}
