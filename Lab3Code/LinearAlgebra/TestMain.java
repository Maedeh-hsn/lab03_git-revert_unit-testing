//Maedeh Hassani (1942575)

public class TestMain {
    public static void main(String[] args) {

        Vector3d vec1 = new Vector3d(1, 2, 3);
        Vector3d vec2 = new Vector3d(4, 5, 1);

        System.out.println("Magnitude Method for vec1= " + vec1.magnitude());
        System.out.println("Magnitude Method for vec2= " + vec2.magnitude());
        System.out.println("DotProduct Method for vec1 & vec2 = " + vec1.dotProduct(vec2));
        System.out.println("Add Method for vec1 & vec2 = " + vec1.add(vec2));
        System.out.println(vec1);
    }
}
