//Maedeh Hassani (1942575)

public class Vector3d { // change the public to final
    private double x;
    private double y;
    private double z;

    // constructor
    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // get methods
    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    // calculate the magnitude of these vectors
    public double magnitude() {
        double magnitude;
        magnitude = Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2);
        return Math.sqrt(magnitude);
    }

    // return the dot product of the two vectors
    public double dotProduct(Vector3d vc1) {
        double dotted = ((vc1.getX() * this.x) + (vc1.getY() * this.y) + (vc1.getZ() * this.z));
        return dotted;
    }

    // add 2 vectors
    public Vector3d add(Vector3d vc1) {
        double newX = vc1.getX() + this.x;
        double newY = vc1.getY() + this.y;
        double newZ = vc1.getZ() + this.z;
        Vector3d vc3 = new Vector3d(newX, newY, newZ);
        return vc3;
    }

    // toString Method
    public String toString() {
        return "x =" + this.x + ", Y= " + this.y + ", Z= " + this.z;
    }
}